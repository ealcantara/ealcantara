# Enrique Alcantara’s README

Hi, I’m a Staff Frontend Engineer at GitLab. This page aims to share with others relevant information about my current focus at GitLab, my working style, and (why not?) myself. 

## Currently at GitLab

- My current timezone is UTC-4000 (Atlantic Time).
- I’m a member of the [Create::IDE team](https://about.gitlab.com/handbook/engineering/development/dev/create/ide/).
- My main areas of focus are [Remote Development](https://gitlab.com/groups/gitlab-org/-/epics/10382) and the [Web IDE](https://gitlab.com/groups/gitlab-org/-/epics/1469).
- To a lesser extent, I support feature development in the [GitLab VSCode Workflow extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension/).
- I have domain expertise in other areas like the [Rich Text Editor](https://docs.gitlab.com/ee/development/fe_guide/content_editor.html), [GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html), the [diagrams.net](http://diagrams.net) editor, and [GitLab UI](https://gitlab.com/gitlab-org/gitlab-ui).
- I'm a member of the [a11y working group](https://about.gitlab.com/company/team/structure/working-groups/product-accessibility/). Our focus in achieving 
[WCAG2.1](https://www.w3.org/TR/WCAG21/) conformance in the GitLab application and related products.

## About me

- I was born and raised in Santo Domingo, Dominican Republic.
- My *typical* workday starts at 9:00 and finishes at 17:00. I usually rearrange my day If I need to run a personal errand, I’m relocating, or I just need time off.
- Outside of work, I enjoy doing complex cooking projects, hosting dinner parties, traveling, and outdoor activities with friends.

## At work

### Working values

- I’m a results-driven person, and I always develop a strong sense of ownership and urgency over the projects that I’m working on.
- The *best* solution is more important than *my* solution. Ego often hinders progress.
- I aspire to be transparent at work. Transparency is paramount in challenging situations.
- I prefer iteration and tenacity over perfection.
- I strongly believe in creating a workplace that works for everyone and asynchronous communication is a basic tenet of this value.
- Time off is sacred: Setting and respecting boundaries for time outside of work is essential.

### Communication preferences

- I pick work from my [To-Do](https://docs.gitlab.com/ee/user/todos.html) list. @Mention me if you want me to pay attention to something.
- I answer Slack messages within 1 to 3 hours.
- I often start a discussion with an issue. If the discussion becomes too complex, I’ll propose jumping on a video call.
- I prefer meetings with clear goals and time-bound by an agenda defined at least a day ahead of time. As a slow thinker, I appreciate having a time frame where I can prepare for a discussion. It makes my contributions more useful.
- I feel more comfortable on 1-1s than on group calls, probably due to being introverted. 
